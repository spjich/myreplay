package main

import (
	"bytes"
	"fmt"
	"time"
)

func main() {
	//str := "POST /mock/alarm HTTP/1.1\r\n"
	//str = str + "User-Agent: curl/7.29.0\r\n"
	//str = str + "Host: localhost:8000\r\n"
	//str = str + "Accept: */*\r\n"
	//str = str + "Content-Type:application/json\r\n"
	//str = str + "Content-Length: 9\r\n\r\n"
	//str = str + "{1111111}"
	input := NewRAWInput("0.0.0.0:8000", EnginePcap, true, time.Millisecond*200, "X-Real-IP", "", "", 0)
	defer input.Close()
	http_output := NewHTTPOutput("http://10.100.23.228:9090", &HTTPOutputConfig{Debug: false, TrackResponses: true, Timeout: 10 * time.Second})
	buf := make([]byte, 5*1024*1024)
	for {
		nr, er := input.Read(buf)
		if er != nil {
			fmt.Println(er.Error())
			break
		}
		if nr > 0 && len(buf) > nr {
			payload := buf[:nr]
			meta := payloadMeta(payload)
			if len(meta) < 3 {
				fmt.Println("[EMITTER] Found malformed record")
				continue
			}
		}
		payload := buf[:nr]
		headSize := bytes.IndexByte(payload, '\n') + 1
		header := payload[:headSize]
		body := payload[headSize:]
		fmt.Println("收到消息 header|", string(header))
		fmt.Println("收到消息 body|", string(body))
		http_output.Write(payload)
	}
}
